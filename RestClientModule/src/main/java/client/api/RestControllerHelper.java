package client.api;

import java.io.IOException;
import java.util.Collections;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import client.api.config.CustomException;
import client.api.config.RestControllerErrorHandler;
import client.api.config.RestControllerRequestInterceptor;

public class RestControllerHelper {
	
	public <T> T get(String url, Class<T> classType) throws CustomException{
		RestTemplate rest = new RestTemplate();
		// Define interceptor (which can add headers for example)
		rest.setInterceptors(Collections.singletonList(new RestControllerRequestInterceptor()));
		// Define error Handler to log error for exampla
		rest.setErrorHandler(new RestControllerErrorHandler());
		try {
			return rest.getForObject(url, classType);
		}catch (HttpServerErrorException e) {
			// TODO create class to check status code error and convert to right exception
			ObjectMapper om = new ObjectMapper();
			CustomException d = null;
			try {
				// Map the response body exception to my own exception class
				d = om.readValue(e.getResponseBodyAsString(), CustomException.class);
			} catch (JsonParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (JsonMappingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw d;
		}
	}
	
	// prefer other method
	public <T> ResponseEntity<T> getForEntity(String url, Class<T> classType) {
		RestTemplate rest = new RestTemplate();
		ResponseEntity<T> res = rest.getForEntity(url, classType);
		return res;
	}

}
