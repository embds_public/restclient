package client.api;

import java.util.Collections;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import client.api.config.CustomException;
import client.api.config.RestControllerRequestInterceptor;

@RestController
@RequestMapping("/client")
public class ClientController {
	
	
	
	@GetMapping("work")
	public String getTestResponse() {
		RestTemplate rest = new RestTemplate();
		return rest.getForObject("http://localhost:8080/test/work", String.class);
	}
	
	@GetMapping("exception")
	public String getException() {
		try {
		RestTemplate rest = new RestTemplate();
		
		rest.setInterceptors(Collections.singletonList(new RestControllerRequestInterceptor()));
		
		return rest.getForObject("http://localhost:8080/test/exception", String.class);
		}catch (HttpServerErrorException e) {
			String message = e.getResponseBodyAsString();
			System.out.println(message);
			return message;
		}
	}
	
	@GetMapping("excep")
	public String getException1() throws CustomException {
		
		RestControllerHelper rest = new RestControllerHelper();
		String res = rest.get("http://localhost:8080/test/exception", String.class);
		return res;
	}

	@GetMapping("excep/entity")
	public ResponseEntity<String> getExceptionEntity() throws CustomException {
		
		RestControllerHelper rest = new RestControllerHelper();
		ResponseEntity<String> res = rest.getForEntity("http://localhost:8080/test/exception", String.class);
		return res;
	}

}
