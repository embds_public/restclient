package client.api.config;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;

// https://stackoverflow.com/questions/38093388/spring-resttemplate-exception-handling?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

public class RestControllerErrorHandler extends DefaultResponseErrorHandler { // or ResponseErrorHandler to redifine hasError

	@Override
	public void handleError(ClientHttpResponse error) throws IOException {
		
	}


}
