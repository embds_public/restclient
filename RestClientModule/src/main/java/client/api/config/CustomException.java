package client.api.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CustomException extends Exception {
	
	private static final long serialVersionUID = 2286952733600911951L;

	private String errorCode;
	
	private String message;

	private CustomException() {
		
	}
	
	public CustomException(String errorCode, String message) {
		super();
		this.errorCode = errorCode;
		this.message = message;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getMessage() {
		return message;
	}
	
	

}
