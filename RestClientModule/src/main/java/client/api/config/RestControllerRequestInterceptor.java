package client.api.config;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

// source : http://javaninja.net/2015/12/spring-resttemplate-basic-authentication/

public class RestControllerRequestInterceptor implements ClientHttpRequestInterceptor {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private final static String privateKey = "thatIs0urK3y!";
	
	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		// Request interceptor
		HttpHeaders headers = request.getHeaders();
        logger.debug("privateKey : {}", privateKey);
        headers.add("privateKey", privateKey);
        // Do call and save the response
        ClientHttpResponse response = execution.execute(request, body);
        // Response interceptor
        
        System.out.println(response.getBody().read());
        System.out.println(response.getStatusCode());
        
        return response;
	}

}
